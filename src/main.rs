use std::{collections::HashMap, path::PathBuf, process::Command};

use color_eyre::eyre::{bail, Context};
use serde::{Deserialize, Serialize};
use structopt::StructOpt;
use thiserror::Error;
use variable::Variableable;

pub mod variable;

#[derive(Error, Debug)]
enum Error {
    #[error("No {theme} for {path}")]
    NoSuchTheme { path: String, theme: String },
    #[error("Variable substitution error")]
    VariableError(#[from] variable::Error),
    #[error("No {theme} listed in the theme commands map")]
    NoSuchThemeCommandList { theme: String },
}

type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ChangedFile {
    to: String,
    from: HashMap<String, String>,
    /// Whether or not to use a symlink for setting the target file. Defaults
    /// to true if unspecified
    use_symlink: Option<bool>,
}

impl ChangedFile {
    fn apply_variables(&self, variables: &HashMap<String, String>) -> Result<Self> {
        Ok(Self {
            to: self.to.substitute(&variables)?,
            from: self.from.substitute(&variables)?,
            use_symlink: self.use_symlink,
        })
    }
}

#[derive(Clone, Serialize, Deserialize)]
struct Config {
    pub vars: HashMap<String, String>,
    changed_files: Vec<ChangedFile>,
    theme_commands: HashMap<String, Vec<String>>,
    update_commands: Vec<String>,
}

impl Config {
    pub fn vars(&self) -> Result<HashMap<String, String>> {
        self.vars
            .substitute(&self.vars)
            .map_err(Error::VariableError)
    }
    pub fn changed_files(&self) -> Result<Vec<ChangedFile>> {
        self.changed_files
            .iter()
            .map(|file| file.apply_variables(&self.vars()?))
            .collect()
    }

    pub fn update_commands(&self) -> Result<Vec<String>> {
        self.update_commands
            .iter()
            .map(|s| s.substitute(&self.vars()?).map_err(Error::VariableError))
            .collect::<Result<Vec<String>>>()
    }

    pub fn theme_commands(&self) -> Result<HashMap<String, Vec<String>>> {
        self.theme_commands
            .iter()
            .map(|(theme, cmd)| {
                Ok((
                    theme.clone(),
                    cmd.iter()
                        .map(|s| s.substitute(&self.vars()?).map_err(Error::VariableError))
                        .collect::<Result<Vec<String>>>()?,
                ))
            })
            .collect()
    }
}

#[derive(StructOpt)]
struct Opt {
    /// Path to custom config file, if unspecified, uses $XDG_CONFIG/themer/config.toml
    #[structopt(short, long)]
    pub config_file: Option<PathBuf>,
    /// Which theme to apply. Must be listed in the `themes` variable in the config
    #[structopt()]
    pub theme: String,
}

fn main() -> color_eyre::eyre::Result<()> {
    color_eyre::install()?;
    let opts = Opt::from_args();

    let xdg_dirs = xdg::BaseDirectories::with_prefix("themer")?;
    let xdg_config_path = xdg_dirs.place_config_file("config.toml")?;
    let config_path = opts.config_file.unwrap_or_else(|| xdg_config_path);

    let config_file = std::fs::read_to_string(&config_path)
        .with_context(|| format!("Failed to read config in {}", config_path.to_string_lossy()))?;

    let config = toml::from_str::<Config>(&config_file)
        .with_context(|| format!("Failed to parse config file"))?;

    let theme = opts.theme;

    // Collect all the file changes we want to make so we can report errors before
    // things go wrong
    let mut to_copy = vec![];
    let mut to_symlink = vec![];
    for file in config.changed_files()? {
        let target_file = file.to;
        let from_file = file
            .from
            .get(&theme)
            .ok_or_else(|| Error::NoSuchTheme {
                path: target_file.clone(),
                theme: theme.clone(),
            })?
            .clone();

        // If the file is a symlink, we need to check if the file exists but isn't a
        // symlink. In that case, we throw an error because we later need to delete the
        // file and we don't want to delete a sensitive file
        if file.use_symlink.unwrap_or(true) {
            let exists = match std::fs::symlink_metadata(&target_file) {
                Ok(meta) => {
                    if !meta.file_type().is_symlink() {
                        bail!("{} is not a symlink but you requested it to be changed as one. Note, set `use_symlink = false` if you want to copy the file instead", target_file)
                    }
                    true
                }
                // An error means we (probably) are dealing with a file that does not
                // exist (or we may not have permission to read it). We'll move on either way
                Err(_) => false,
            };

            to_symlink.push((target_file, from_file, exists))
        } else {
            to_copy.push((target_file, from_file))
        }
    }

    let theme_commands = config.theme_commands()?;
    let mut commands = theme_commands
        .get(&theme)
        .cloned()
        .ok_or_else(|| Error::NoSuchThemeCommandList { theme })?;

    commands.append(&mut config.update_commands()?);

    for (target, from, exists) in to_symlink {
        if exists {
            std::fs::remove_file(&target)
                .with_context(|| format!("Failed to remove {}", target))?;
        }
        std::os::unix::fs::symlink(from, &target)
            .with_context(|| format!("Failed to create symlink to {}", target))?;
    }

    for (target, from) in to_copy {
        std::fs::copy(&from, &target)
            .with_context(|| format!("Failed to copy {} to {}", from, target))?;
    }

    for cmd in commands {
        Command::new("sh")
            .arg("-c")
            .arg(&cmd)
            .status()
            .with_context(|| format!("Failed to run {}", cmd))?;
    }

    Ok(())
}
