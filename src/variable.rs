use std::collections::HashMap;

use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
pub enum Error {
    #[error("Unexpected character {got}, in variable starting at {in_var}")]
    UnexpectedChar { got: char, in_var: usize },
    #[error("Expected {{ to match $ at {at}")]
    ExpectedBrace { at: usize },
    #[error("Undefined variable {what}, between {start} and {end}")]
    UndefinedVariable {
        what: String,
        start: usize,
        end: usize,
    },
    #[error("Missing closing {{ for variable starting at {at}")]
    UnterminatedVarialbe { at: usize },
    #[error("In map key {key}")]
    InMapKey {
        key: String,
        #[source]
        inner: Box<Error>,
    },
    #[error("In map member {key}")]
    InMapMember {
        key: String,
        #[source]
        inner: Box<Error>,
    },
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(PartialEq)]
pub enum ReplaceState {
    Text,
    /// We've seen a $ and are looking for {. The $ was the .0th char
    StartVariable(usize),
    /// we're currently parsing a variable starting at .0 and the curent
    /// variable name is .1
    Variable(usize, String),
}

pub trait Variableable {
    type Output;
    fn substitute(&self, vars: &HashMap<String, String>) -> Result<Self::Output>;
}

impl Variableable for String {
    type Output = Self;
    fn substitute(&self, vars: &HashMap<String, String>) -> Result<Self> {
        let mut result = String::new();
        let mut state = ReplaceState::Text;
        for (i, c) in self.chars().enumerate() {
            state = match state {
                ReplaceState::Text => {
                    if c == '$' {
                        ReplaceState::StartVariable(i)
                    } else {
                        result.push(c);
                        ReplaceState::Text
                    }
                }
                ReplaceState::StartVariable(at) => {
                    if c == '{' {
                        ReplaceState::Variable(at, "".to_string())
                    } else {
                        return Err(Error::ExpectedBrace { at });
                    }
                }
                ReplaceState::Variable(at, mut current) => {
                    if c == '}' {
                        if let Some(val) = vars.get(&current) {
                            result += val
                        } else {
                            return Err(Error::UndefinedVariable {
                                what: current,
                                start: at,
                                end: i,
                            });
                        }
                        ReplaceState::Text
                    } else if c.is_alphanumeric() || c == '_' {
                        current.push(c);

                        ReplaceState::Variable(at, current)
                    } else {
                        return Err(Error::UnexpectedChar { got: c, in_var: at });
                    }
                }
            }
        }

        match state {
            ReplaceState::Text => Ok(result),
            ReplaceState::StartVariable(at) => Err(Error::UnterminatedVarialbe { at }),
            ReplaceState::Variable(at, _) => Err(Error::UnterminatedVarialbe { at }),
        }
    }
}

impl Variableable for HashMap<String, String> {
    type Output = Self;
    fn substitute(&self, vars: &HashMap<String, String>) -> Result<Self::Output> {
        self.iter()
            .map(|(key, value)| {
                Ok((
                    key.substitute(vars).map_err(|e| Error::InMapKey {
                        key: key.clone(),
                        inner: Box::new(e),
                    })?,
                    value.substitute(vars).map_err(|e| Error::InMapKey {
                        key: key.clone(),
                        inner: Box::new(e),
                    })?,
                ))
            })
            .collect::<Result<HashMap<_, _>>>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn variable_substitution_works() {
        let input = "abc${abc}abc".to_string();

        let vars = vec![("abc".to_string(), "def".to_string())]
            .into_iter()
            .collect::<HashMap<_, _>>();

        assert_eq!(input.substitute(&vars), Ok("abcdefabc".to_string()));
    }

    #[test]
    fn undefined_variable_causes_error() {
        let input = "abc${abc}abc".to_string();

        let vars = HashMap::new();

        assert_eq!(
            input.substitute(&vars),
            Err(Error::UndefinedVariable {
                what: "abc".to_string(),
                start: 3,
                end: 8
            })
        );
    }

    #[test]
    fn variable_with_invalid_characters_causes_error() {
        let input = "abc${$}abc".to_string();

        let vars = HashMap::new();

        assert_eq!(
            input.substitute(&vars),
            Err(Error::UnexpectedChar {
                got: '$',
                in_var: 3
            })
        );
    }

    #[test]
    fn unterminated_variable_causes_error() {
        let input = "abc${abc".to_string();

        let vars = HashMap::new();

        assert_eq!(
            input.substitute(&vars),
            Err(Error::UnterminatedVarialbe { at: 3 })
        );
    }

    #[test]
    fn missing_brace_causes_error() {
        let input = "abc$abc".to_string();

        let vars = HashMap::new();

        assert_eq!(input.substitute(&vars), Err(Error::ExpectedBrace { at: 3 }));
    }
}
